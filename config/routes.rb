Rails.application.routes.draw do
  get 'scrapper/index'

  post 'scrapper/search'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
