class ScrapperController < ApplicationController
  def index
  end

  def search
    if params[:username] && !params[:username].blank?
     @username = params[:username]
    else
     raise MyApp::MissingFieldError
    end
    require 'open-uri'
    @url = "https://www.instagram.com/" + @username
    file = open(@url)
    data = file.read
    data = data.split("<script type=\"text/javascript\">window._sharedData =")
    data = data[1].split(";</script>")[0]
    data = JSON.parse data
    @user = data['entry_data']['ProfilePage'][0]['graphql']['user']
  end
end
